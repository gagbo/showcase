# SPDX-FileCopyrightText: 2022 Gerry Agbobada <git@gagbo.net>
#
# SPDX-License-Identifier: CC0-1.0

{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.xvfb-run
    pkgs.i3
    pkgs.scrot

    # keep this line if you use bash
    pkgs.bashInteractive
  ];
}
