;; SPDX-FileCopyrightText: 2022 Gerry Agbobada <git@gagbo.net>
;;
;; SPDX-License-Identifier: MIT

(director-bootstrap
 :packages '()
 :load-path '("util"))

(director-run
 :version 1
 :before-start (lambda ()
                 (cd (getenv "PWD")))
 :steps '(;; Open a markdown file and remove squiggles
          (:type " .")
          (:type "README.md\r")
          (:type " ts")

          ;; Toggle the terminal once and quit
          ;; for multi-vterm shenanigans
          (:type " ot")
          (:type [escape])
          (:type " ww")

          ;; Split and open a programming file
          (:type " ws")
          (:type " .")
          (:type "util/setup_screenshot.el\r")

          ;; Open project tree
          (:type " op"))
 :typing-style 'human
 :delay-between-steps 0.5
 :after-end (lambda ()
              (call-process "scrot"
                  nil nil nil
                  "-D" ":99" "-F" "virtual-screen.png")
              (princ (format "Sending signal to %s\n" (getenv "SHOWCASE_PID")))
              (signal-process (string-to-number (getenv "SHOWCASE_PID")) 'SIGUSR1))
 :on-error (lambda ()
             (princ "Failure - Messages:\n")
             (princ (buffer-string (messages-buffer)))
             (signal-process (string-to-number (getenv "SHOWCASE_PID")) 'SIGUSR1)))
