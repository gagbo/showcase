; SPDX-FileCopyrightText: 2022 Gerry Agbobada <git@gagbo.net>
;
; SPDX-License-Identifier: CC0-1.0

(use-modules (guix packages)
             (gnu packages)
             (gnu packages emacs)
             (gnu packages xdisorg)
             (gnu packages xorg))

(packages->manifest
(list ;; The Basics
  xvfb-run
  scrot
  i3
  emacs))

