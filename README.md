<!--
SPDX-FileCopyrightText: 2022 Gerry Agbobada <git@gagbo.net>

SPDX-License-Identifier: MIT
-->

# Showcase: headless and automated screenshot app

Utility that runs a graphical application like emacs and gets a screenshot.

This has been tested on Wayland, with Emacs, as it's probably going to be used
to generate screenshots for various themes.

```sh
./showcase
xdg-open virtual_screen.png
```

## Dependencies
- xvfb
- i3
- i3bar
- scrot

## Usecases
- Produce screenshots from a GUI app in a CI setting
- Produce minimal reproducible examples for UI/layout related issues
